const express = require('express');
const http = require('http');
const https = require('https');
const url = require('url');
const app = express();
const moment = require('moment');
const port = process.env.PORT || 8080;
const server = app.listen(port, function () {
    console.log(`Server listening on port ${port}!`)
})
const io = require('socket.io')(server);

app.get('/history', function (req, res) {
    res.json(lastmessages)
})

app.use(express.static('public'))
app.use('/modules', express.static('node_modules'));

let halt = false;
let connectedUsers = 0;
// Handle connection
io.on('connection', function (socket) {
    connectedUsers++
    socket.on('disconnect', () => {
        connectedUsers--;
    });
    socket.emit('goturl', lastmessages['goturl']);
    socket.on('testURL', BeginTestURL);
});
testcount = 0;
let inProgress = false;
let lastmessages = {};

sendMessage = (event, data) => {
    if (!lastmessages[event]) lastmessages[event] = [];

    lastmessages[event].unshift(data);
    lastmessages[event] = lastmessages[event].slice(0,10);
    io.sockets.emit(event, data);
}

BeginTestURL = (data) => {
    if (inProgress) {
        halt = true;
        setTimeout(function () {
            BeginTestURL(data);
        }, 500);
        return;
    }
    if (!data.url) {halt = true; return;}
    testcount = 0;
    inProgress = true;
    halt = false;
    TestURL(data);
}

TestURL = (data) => {
    if (halt) {
        inProgress = false;
        return;
    }
    testcount++;
    let retval = {success: false, message: null, count: testcount, time: moment().format(), senddata: data};
    let method = null;
    const protocol = url.parse(data.url).protocol;
    switch (protocol) {
        case "http:": {
            method = http;
            break;
        }
        case "https:": {
            method = https;
            break;
        }
    };
    try {
        method.get(data.url, (res) => {
            if (halt) {
                inProgress = false;
                return;
            }
            const statusCode = res.statusCode;
            let error;
            if (statusCode === 200) {
                retval.success = true;
            } else {
                retval.success = false;
                retval.message = `Status Code: ${statusCode}`;
            }
            sendMessage('goturl', retval);
            if (retval.success && !halt) {
                setTimeout(function () {
                    TestURL(data);
                }, data.wait);
            } else {
                testcount = 0;
                inProgress = false;
                return;
            }
        }).on('error', (e) => {
            retval.success = false;
            retval.message = `Got error: ${e.message}`;
            sendMessage('goturl', retval);
            testcount = 0;
            halt = true;
            inProgress = false;
            return;
        });
    } catch (ex) {
        sendMessage('goturl', retval);
        testcount = 0;
        halt = true;
        inProgress = false;
        return;
    }
}

process.on('uncaughtException', function (err) {
    console.log(err);
}); 