
$("#testurl").on('input', function () {
    StopClient();
});
function seturl(url) {
    StopClient();
    $('#testurl').val(url)
}
function serverorclientchanged(clientorserver){
    $('.result').hide();
    $('#'+clientorserver+'result').show();
    if (clientorserver == 'client') {
        ClientSelected();
    } else {
        ServerSelected();
    }
}
function ClearOutput(clientorserver) {
    if (clientorserver == 'client') {
        $('#clientresult').empty();
    } else {
        $('#serverresult').empty();
    }
}
function Output(clientorserver, object, dontappend) {
    var string = object;
    try {
        string = JSON.stringify(object);
    }
    catch (err) {

    }
    var p = $('<p />').html(string);
    if (dontappend) {
        ClearOutput(clientorserver);
    }
    
        $('#'+clientorserver+'result').prepend(p);
        $('#'+clientorserver+'result').html($('#'+clientorserver+'result').children().slice(0,10))
    
}
function GetURL() {
    return $('#testurl').val();
}
function GetWait() {
    var wait = $('#waittime').val();
    if (!wait) wait = 1000;
    return wait;
}
$('#testbtn').click(function () {
    if (clientorserver() == 'client') {
        StartClient();
    } else {
        StartServer();
    }
});

$('#stopbtn').click(function () {
    if (clientorserver() == 'client') {
        StopClient();
    } else {
        StopServer();
    }
});

function clientorserver() {
    return $('#serverorclient label.active input')[0].id
}
function requestnotify() {
    if (!("Notification" in window)) {
        return;
    }
    Notification.requestPermission();
}
requestnotify();
function notifyMe(message) {
    if (!("Notification" in window) || Notification.permission !== "granted") {
        return;
    }
    new Notification(message);
}