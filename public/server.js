var socket = io('/');

function StartServer() {
    socket.emit('testURL', { url: GetURL(), wait: GetWait() });
}

function StopServer() {
    socket.emit('testURL', { url: null });
}

function ServerSelected() {
    
    // $.getJSON( "/history", function( data ) {
    //     Output('server', data);
    // });
}

socket.on('goturl', function (data) {
    if (data && data.length) {
        for (var i = data.length; i > -1; i--){
            Output('server', data[i]);
        }
    } else {
        Output('server', data);
    }
});
