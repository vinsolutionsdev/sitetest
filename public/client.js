var i = 1;
var clientRunning = false;
var clientStopping = false;
function StartClient() {
    if (clientRunning) return;
    i = 1;
    clientRunning = true;
    $('#clienttestarea').empty();
    Output('client', "testing " + GetURL() + " until fail");
    getURLLooped();
}

function ClientSelected() {
    // ClearOutput('client');
}

function StopClient() {
    if (!clientRunning) {
        clientStopping = false;
        return;
    }
    clientStopping = true;

    setTimeout(function () {
        StopClient();
    }, 500);
}

function StoppingStateClient() {
    if (clientStopping) {
        clientRunning = false;
        return true;
    }
}

function getURLLooped() {
    if (StoppingStateClient()) return;
    Output('client', "test number " + i + " is okay");
    document.title = 'test ' + i

    var linktest = $("<link/>")
        .on('load', recource_success)
        .on('error', recource_error)
        .attr("href", GetURL() + "?a=a&_=" + (new Date).getTime()).attr("rel", "preload");
    $('#clienttestarea').append(linktest);
}

function recource_success(data) {
    $('#clienttestarea').empty();

    if (StoppingStateClient()) return;
    i++;
    setTimeout(function () {
        getURLLooped();
    }, GetWait());
}
function recource_error(data) {
    $('#clienttestarea').empty();
    Output('client', "ERROR SEE CONSOLE")
    StopClient();
    StoppingStateClient();
    document.title = 'ERROR';
    if (i > 2) {
        notifyMe("ERROR WHEN LOADING " + GetURL());
    }
}